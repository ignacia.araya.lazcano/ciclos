def rut_8(rut):
    list_numeros = []
    for numero in rut:
        list_numeros.append(int(numero))
    serie_numerica = [2,3,4,5,6,7,2,3]    
    # print(list_numeros)
    # print(serie_numerica)

    i = 0
    mult = 0
    #list_numeros.reverse()
    
    # print(list_numeros)
    # print(serie_numerica) 
      
    for numero in reversed(list_numeros):
        a = (numero * serie_numerica[i])
        mult = a + mult
        i = i + 1 
    dv = ((11 - (mult % 11)) % 11)
    return dv


while True:
    rut = input("Ingresa tu RUT sin puntos ni digito verificado: ")
    if len(rut) == 7 or len(rut) == 8:
        dv = rut_8(rut)
        if dv == 10:
            print("Su digito verificador es K")
            break
        elif dv == 11:
            print("Su digitor verificador es 0")
            break
        else:
            print(f"Su digito verificador es {dv}")
            break
    else:
        print("Ingresa un rut valido! ")